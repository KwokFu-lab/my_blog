# 导入内建的User模型
from django.contrib.auth.models import User
from django.db import models
# timezone 用于处理时间相关事务
from django.utils import timezone


# Create your models here.
# 每当你修改了models.py文件，都需要用makemigrations和migrate这两条指令迁移数据。
# python manage.py makemigrations
# python manage.py migrate
# 在迁移之后，Model的编写就算完成了

# 博客文章数据模型
class ArticlePost(models.Model):
    # 文章作者。参数 on_delete 用于指定数据删除的方式
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    # 文章标题。models.CharField 为字符串字段，用于保存较短的字符串，比如标题
    title = models.CharField(max_length=100)
    # 文章正文，保存大量文本使用TextField
    body = models.TextField()
    # 文章创建时间，参数default=timezone.now 指定其在创建数据时将默认写入当前的时间
    created = models.DateTimeField(default=timezone.now)
    # 文章更新时间，参数auto_now=True指向每次数据更新时自动写入当前时间
    updated = models.DateTimeField(auto_now=True)

    # 内部类 class Meta 用于给 model 定义元数据
    # 这不是字段，是为了规范类的行为
    # 表明了每当我需要取出文章列表，作为博客首页时，按照-created（即文章创建时间，负号标识倒序）来排列，
    # 保证了最新文章永远在最顶部位置
    class Meta:
        ordering = ('-created',)

    # 函数 __str__ 定义当调用对象的 str() 方法时的返回值内容
    def __str__(self):
        return self.title


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
